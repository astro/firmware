{ openocd, fetchFromGitHub, autoreconfHook, git, which }:
openocd.overrideAttrs (oa: {
  src = fetchFromGitHub {
    owner = "maximmbed";
    repo = "openocd";
    rev = "e71ac88c9dbfa4ee1405d7a86376119dcc887ed1";
    sha256 = "18yc1wyclmjxqg6jilfcm60hi01pgqc4dilsmksqbhg23m6x4ycw";
    fetchSubmodules = true;
  };
  nativeBuildInputs = oa.nativeBuildInputs ++ [
    autoreconfHook
    git
    which
  ];
  enableParallelBuilding = true;
})
