{ fetchFromGitHub, pkgsCross, python3, ninja, meson, gcc }:

let
  micropython = fetchFromGitHub {
    owner = "micropython";
    repo = "micropython";
    rev = "62f004ba424920a01e60c7a9a064b8ec9cd69c12";
    sha256 = "0bhbdw4zlk4yr798x8k9pk3q85xnbqj1f2362l65wv1815ajzx5l";
    fetchSubmodules = true;
  };
in
  pkgsCross.armhf-embedded.stdenv.mkDerivation {
    name = "card10";
    src = ./..;
    postPatch = ''
      substituteInPlace card10-cross.ini \
        --replace arm-none-eabi arm-none-eabihf
      rmdir lib/micropython/micropython
      ln -s ${micropython}/ lib/micropython/micropython
    '';
    nativeBuildInputs = [ ninja meson python3 gcc ];
    mesonFlags = [
      "--cross-file card10-cross.ini"
      "--optimization s"
    ];
    ninjaFlags = [ "-v" ];
  }
