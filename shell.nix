with import <nixpkgs> {};
pkgsCross.armhf-embedded.stdenv.mkDerivation {
  name = "env";
  buildInputs = [ ninja meson python3 gcc bashInteractive ];
}
