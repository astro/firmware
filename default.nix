{ pkgs ? import <nixpkgs> {},
}:
with pkgs;
let
  firmware = callPackage ./nix/firmware.nix {};
  openocd = callPackage ./nix/openocd.nix { };
in
{
  openocd = stdenv.mkDerivation {
    name = "openocd-card10";
    src = ./openocd;
    phases = [ "unpackPhase" "installPhase" ];
    buildInputs = [ openocd makeWrapper ];
    installPhase = ''
      mkdir -p $out/bin $out/share
      cp -ar . $out/share/openocd
      makeWrapper ${openocd}/bin/openocd $out/bin/openocd-card10 \
        --add-flags "-f $out/share/openocd/scripts/interface/cmsis-dap.cfg" \
        --add-flags "-f $out/share/openocd/scripts/target/max32665.cfg"
    '';
  };
  inherit firmware;
}
